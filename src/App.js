import React from 'react';
import './App.css';

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  const tableRows = weekdays.map((weekday, index) => (
    <tr key={index}>
      <td>{weekday}</td>
    </tr>
  ));

  return (
    <div className="App">
      <div className="upperHeader">W13 Weekdays in table</div>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody>
          {tableRows}
        </tbody>
      </table>
    </div>
  );
}

export default App;
